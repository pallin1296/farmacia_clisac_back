import datetime
import os
import random
import math

from datetime import timedelta
from flask import Flask, request
from flask_cors import CORS
from flask_jwt import JWT, jwt_required,current_identity
from sqlalchemy.util.queue import Empty
from werkzeug.security import safe_str_cmp
from sqlalchemy import cast, and_, not_, DATE, join, func, or_

from config import server_connection

from logging.config import dictConfig
from dataclasses import dataclass
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import *
from sqlalchemy.ext.hybrid import hybrid_property

db = SQLAlchemy()

import pytz
tz = pytz.timezone('America/Mexico_city')

import time
import atexit


def authenticate(username, password):
    #user=db.session.execute("SELECT * from usuario where ").first()
    user = usuario.query.filter_by(username=username).first()
    if user and safe_str_cmp(user.password.encode('utf-8'), password.encode('utf-8')):
        return user


def identity(payload):
    user_id = payload['identity']
    return usuario.query.filter_by(id=user_id).first()


app = Flask(__name__)
app.debug = True
app.config['SECRET_KEY'] = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzb21lIjoicGF5bG9hZCJ9.4twFt5NiznN84AWoo1d7KO1T_yoc0Z6XOpOVswacPZg'
#app.config['JWT_EXPIRATION_DELTA'] = timedelta(hours=2)
app.config['SQLALCHEMY_DATABASE_URI'] = server_connection
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db.init_app(app)
CORS(app)

jwt = JWT(app, authenticate, identity)


@jwt.auth_response_handler
def customized_response_handler(access_token, identity):
    return {'access_token': access_token.decode('utf-8'), 'role': identity.idrol,'username':identity.username}


@app.route('/')
def signature():
    return {
        "appId": "farmacia-clisac",
        "version": "VERSION 1.0"
    }

#@jwt_required()

# # # # # # PRODUCTO # # # # # 
@app.route('/public/producto/seleccionar-producto', methods=['POST'])
def seleccionar_producto():
    res = Response()
    try:

        now = datetime.datetime.now(tz)
        content = request.json
        infoProd={}
        
        producto= db.session.execute("select idproducto,producto,precioproveedor,preciocliente,preciopublico,pza from producto where codigo='"+content['codigo']+"' and alta=1").first()

        if producto is not None:
            if producto.pza!=2:
                infoProd={
                    "idProducto":producto.idproducto,
                    "producto":producto.producto,
                    "precioProveedor":float(producto.precioproveedor),
                    "precioCliente":float(producto.preciocliente),
                    "precioPublico":float(producto.preciopublico)
                }
                res["ok"] = True
                res['data']=infoProd
            else:
                res["ok"] = False
                res["msg"] = str("No puede agregar, el producto es un paquete.")

        else:
            res["ok"] = False
            res["msg"] = str("El producto no existe.")

    except Exception as e:
        print(e)
        res["ok"] = False
        res["msg"] = str(e)

    return res

@app.route('/public/producto/obtener-productos')
def obtener_productos():
    res = Response()
    try:

        now = datetime.datetime.now(tz)
        listas=[]
        
        for item in db.session.execute('SELECT prod.idproducto, prod.producto,'\
        ' prod.precioproveedor, prod.preciocliente, prod.preciopublico, prod.alta, prod.pza FROM producto prod order by prod.idproducto DESC').all():
            listas.append({
                "idproducto":item.idproducto,
                "producto":item.producto,
                "precioproveedor":str(item.precioproveedor),
                "preciocliente":str(item.preciocliente),
                "preciopublico":str(item.preciopublico),
                "alta":item.alta,
                "pieza":item.pza
            })

        if listas is None:
            res["ok"] = False
            res["msg"] = str("No Contiene productos.")
            return res
        
        res["ok"] = True
        res['data']=listas

    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res

@app.route('/public/producto/estatus-producto', methods=['POST'])
def actualizar_estatus_producto():
    res = Response()
    try:

        content = request.json
        now = datetime.datetime.now(tz)

        addAlmacen=db.session.execute("UPDATE producto SET alta="+str(content['alta'])+","+'"fechaActualizacion"'+"='"+str(now)+"' WHERE idproducto='"+str(content['idproducto'])+"'")
        db.session.commit()

        res["ok"] = True
        res["msg"] = str("El estatus del producto ha sido actualizado exitosamente.")

    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res

@app.route('/public/producto/obtener-producto-detalle', methods=['POST'])
def obtener_producto_detalle():
    res = Response()
    try:
        content = request.json
        now = datetime.datetime.now(tz)
        producto={}
        listaProductos=[]
        item=db.session.execute('SELECT prod.idproducto, prod.codigo, prod.producto, prod.precioproveedor'\
        ', prod.preciocliente, prod.preciopublico, prod.pza, prod.alta, prod.idcategoria,'\
        'cat.categoria, prod."fechaCreacion", prod."fechaActualizacion",prod.fechacaducidad,prod.lote,prod.tipoadministracion FROM producto as prod,categoria as cat '\
        "WHERE prod.idcategoria=cat.idcategoria and prod.idproducto="+str(content['idproducto'])+" ").first()

        obtenerLista=db.session.execute("select pp.idpaqueteproducto,prod.idproducto,prod.producto, pp.cantidad,prod.precioproveedor,prod.preciocliente,prod.preciopublico "\
                "from paquete_producto pp,producto prod where pp.idproducto="+str(content['idproducto'])+" and "\
                    "prod.idproducto=pp.idproductorelacion").all()

        for prod in obtenerLista:
            listaProductos.append({
                "idPaqueteProducto":prod.idpaqueteproducto,
                "idProducto":prod.idproducto,
                "producto":prod.producto,
                "cantidad":str(prod.cantidad),
                "precioProveedor":str(prod.precioproveedor),
                "precioCliente":str(prod.preciocliente),
                "precioPublico":str(prod.preciopublico)                
            })
        
        producto={
            "idproducto":item.idproducto,
            "codigo":item.codigo,
            "producto":item.producto,
            "precioproveedor":str(item.precioproveedor),
            "preciocliente":str(item.preciocliente),
            "preciopublico":str(item.preciopublico),
            "pieza":item.pza,
            "idcategoria":item.idcategoria,
            "categoria":item.categoria,
            "fechaCreacion":str(item.fechaCreacion),
            "fechaActualizacion":str(item.fechaActualizacion),
            "alta":item.alta,
            "caducidad":item.fechacaducidad,
            "lote":item.lote,
            "tipoadministracion":item.tipoadministracion,
            "listaProductos":listaProductos
            }
        
        res["ok"] = True
        res['data']=producto

    except Exception as e:
        print("error: "+str(e))
        res["ok"] = False
        res["msg"] = str(e)

    return res

@app.route('/public/producto/producto-agregar', methods=['POST'])
def producto_agregar():
    res = Response()
    try:

        content = request.json
        now = datetime.datetime.now(tz)

        lote="NULL"
        caducidad="NULL"
        administracion="NULL"

        if content['tipoadministracion'] is not None:
            administracion="'"+content['tipoadministracion']+"'"

        if content['caducidad'] is not None:
            caducidad="'"+content['caducidad']+"'"

        if content['lote'] is not None:
            lote="'"+content['lote']+"'"

        existente=db.session.execute("SELECT idproducto FROM producto where codigo='"+content['codigo']+"'").first()
        db.session.commit()

        if existente is None:
            pieza="1"
            if content['pieza'] is False:
                pieza="2"
            addProd=db.session.execute('INSERT INTO producto('\
        'codigo, producto, precioproveedor, preciocliente, preciopublico, pza, alta, idcategoria, "fechaCreacion", "fechaActualizacion",lote,fechacaducidad,tipoadministracion)'\
        "VALUES ('"+content['codigo']+"', UPPER('"+content['producto']+"'), "+str(content['precioProveedor'])+""\
        ", "+str(content['precioCliente'])+","+str(content['precioPublico'])+", "+pieza+", 1 "\
        ", "+str(content['idCategoria'])+", '"+str(now)+"', '"+str(now)+"',"+lote+","+caducidad+","+administracion+")")
            #db.session.add(query)
            db.session.commit()

            ultimoId=db.session.execute("SELECT idproducto FROM producto order by idproducto desc limit 1").first()
            db.session.commit()

            addProdAlmaPrinc=db.session.execute('INSERT INTO almacen_principal('\
	    'idproducto, minimo, existente, "fechaCreacion", "fechaActualizacion")'\
        "VALUES ("+str(ultimoId.idproducto)+", "+str(content['minimoAlmacen'])+", "+str(content['existente'])+" "\
        ", '"+str(now)+"', '"+str(now)+"')")
            #db.session.add(query)
            db.session.commit()

            for item in db.session.execute("SELECT idalmacen FROM almacen").all():
                addProdAlmSec=db.session.execute('INSERT INTO almacen_producto('\
	    'idproducto,idalmacen, minimo, existente, "fechaCreacion", "fechaActualizacion")'\
        "VALUES ("+str(ultimoId.idproducto)+","+str(item.idalmacen)+", 0, 0"\
        ", '"+str(now)+"', '"+str(now)+"')")
                db.session.commit()

            if content['pieza'] is False:
                for item in content['listaProducto']:

                    addProdPaq=db.session.execute("INSERT INTO paquete_producto(idproducto, fechacreacion, fechaactualizacion, cantidad, idproductorelacion) "\
                        "VALUES ("+str(ultimoId.idproducto)+", '"+str(now)+"', '"+str(now)+"', "+str(item['cantidad'])+", "+str(item['idProducto'])+");")
                    db.session.commit()

        else:
            res["ok"] = False
            res["msg"] = str("Codigo existente.")
            return res

        res["ok"] = True
        res["msg"] = str("Producto Guardado Exitosamente.")

    except Exception as e:
        print(e)
        res["ok"] = False
        res["msg"] = str(e)

    return res

@app.route('/public/producto/producto-editar', methods=['POST'])
def producto_editar():
    res = Response()
    try:

        content = request.json
        now = datetime.datetime.now(tz)

        lote="NULL"
        caducidad="NULL"
        administracion="NULL"

        if content['tipoadministracion'] is not None:
            administracion="'"+content['tipoadministracion']+"'"

        if content['caducidad'] is not None:
            caducidad="'"+content['caducidad']+"'"

        if content['lote'] is not None:
            lote="'"+content['lote']+"'"

        existente=db.session.execute("SELECT idproducto FROM producto where codigo='"+content['codigo']+"'").first()
        db.session.commit()

        if existente.idproducto == content['idproducto'] or existente is None:
            pieza="1"
            if content['pieza'] is False:
                pieza="2"
            editarProducto=db.session.execute("UPDATE producto	SET codigo='"+content['codigo']+"', producto=UPPER('"+content['producto']+"'), "\
            "precioproveedor="+str(content['precioProveedor'])+", preciocliente="+str(content['precioCliente'])+", preciopublico="+str(content['precioPublico'])+", pza="+pieza+", "\
            'idcategoria='+str(content['idCategoria'])+', '+'"fechaActualizacion"'+"='"+str(now)+"',lote="+lote+",fechacaducidad="+caducidad+",tipoadministracion="+administracion+" WHERE idproducto="+str(content['idproducto'])+" ")    
            db.session.commit()

            if content['pieza'] is False:
                deleteProdPaq=db.session.execute("delete FROM paquete_producto where idproducto="+str(content['idproducto'])+";")
                db.session.commit()

                for item in content['listaProducto']:

                    addProdPaq=db.session.execute("INSERT INTO paquete_producto(idproducto, fechacreacion, fechaactualizacion, cantidad, idproductorelacion) "\
                        "VALUES ("+str(content['idproducto'])+", '"+str(now)+"', '"+str(now)+"', "+str(item['cantidad'])+", "+str(item['idProducto'])+");")
                    db.session.commit()
            
        else:
            res["ok"] = False
            res["msg"] = str("Codigo existente.")
            return res

        res["ok"] = True
        res["msg"] = str("Producto Actualizado Exitosamente.")

    except Exception as e:
        print(e)
        res["ok"] = False
        res["msg"] = str(e)

    return res    

@app.route('/public/producto/producto-status', methods=['POST'])
def producto_status():
    res = Response()
    try:

        content = request.json
        now = datetime.datetime.now(tz)

        db.session.execute("UPDATE producto SET alta="+content['status']+" WHERE idproducto="+content['idproducto']+" ")

        db.session.commit()

        
    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res

@app.route('/public/producto/buscar-producto')
def buscar_producto():
    res = Response()
    try:

        content = request.json
        now = datetime.datetime.now(tz)

        listaProducto=[]

        for item in db.session.execute('select prod.codigo ,prod.producto from producto as prod order by prod.idproducto DESC').all():
            listaProducto.append({
                "codigo":item.codigo,
                "producto":item.producto
            })

        res["data"] = listaProducto

    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res

# # # # # almacen # # # # # #

@app.route('/public/almacen-principal/editar-minimo-almacen-principal', methods=['POST'])
def editar_minimo_almacen_principal():
    res = Response()
    content = request.json
    try:

        update=db.session.execute("update almacen_principal set minimo="+str(content['cantidad'])+" where idalmacenprincipal="+str(content['idalmacenprincipal'])+" ")
        db.session.commit()
        
        res["ok"] = True
        res["msg"] = "Minimo Ajustado."

    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res

@app.route('/public/almacen-secundario/editar-minimo-almacen-secundario', methods=['POST'])
def editar_minimo_almacen_secundario():
    res = Response()
    content = request.json
    try:

        update=db.session.execute("update almacen_producto set minimo="+str(content['cantidad'])+" where idalmacenproducto="+str(content['idalmacenproducto'])+" ")
        db.session.commit()
        
        res["ok"] = True
        res["msg"] = "Minimo Ajustado."

    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res    

@app.route('/public/almacen-principal/ajustar-existencia-almacen-principal', methods=['POST'])
def ajustar_existencia_almacen_principal():
    res = Response()
    content = request.json
    try:

        update=db.session.execute("update almacen_principal set existente="+str(content['cantidad'])+" "\
        " where idproducto=(select idproducto from producto where codigo='"+content['codigo']+"')")
        db.session.commit()
        
        res["ok"] = True
        res["msg"] = "Cantidad Ajustada."

    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res

@app.route('/public/almacen-secundario/ajustar-existencia-almacen-secundario', methods=['POST'])
def ajustar_existencia_almacen_secundario():
    res = Response()
    content = request.json
    try:

        update=db.session.execute("update almacen_producto set existente="+str(content['cantidad'])+" "\
        " where idalmacen="+str(content['idalmacen'])+" and idproducto=(select idproducto from producto where codigo='"+content['codigo']+"')")
        db.session.commit()
        
        res["ok"] = True
        res["msg"] = "Cantidad Ajustada."

    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res

@app.route('/public/almacen-principal/agregar-existencia-almacen-principal', methods=['POST'])
def agregar_existencia_almacen_principal():
    res = Response()
    content = request.json
    try:

        update=db.session.execute("update almacen_principal set existente=existente+"+str(content['cantidad'])+" "\
        " where idproducto=(select idproducto from producto where codigo='"+content['codigo']+"')")
        db.session.commit()
        
        res["ok"] = True
        res["msg"] = "Cantidad Agregada."

    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res

@app.route('/public/almacen-secundario/agregar-existencia-almacen-secundario', methods=['POST'])
def agregar_existencia_almacen_secundario():
    res = Response()
    content = request.json
    try:

        update=db.session.execute("update almacen_producto set existente=existente+"+str(content['cantidad'])+" "\
        "where idalmacen="+str(content['idalmacen'])+" and idproducto=(select idproducto from producto where codigo='"+content['codigo']+"')")
        db.session.commit()
        
        res["ok"] = True
        res["msg"] = "Cantidad Agregada."

    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res    

@app.route('/public/almacen-principal/obtener-producto-almacen-principal')
def mostrar_productos_almacen_principal():
    res = Response()
    try:
        lista=[]

        query='select al.idalmacenprincipal,prod.codigo,prod.producto,al.minimo,al.existente '\
        'from producto as prod, almacen_principal as al '\
        'where al.idproducto=prod.idproducto ORDER BY prod.idproducto DESC'

        for item in db.session.execute(query).all():
            lista.append({
                "idalmacenprincipal":item['idalmacenprincipal'],
                "codigo":item['codigo'],
                "producto":item['producto'],
                "minimo": round(float(item['minimo']),2),
                "existente": round(float(item['existente']),2)
            })
        
        res["ok"] = True
        res["data"] = lista

    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res

@app.route('/public/almacen-secundario/obtener-producto-almacen-secundario', methods=['POST'])
def mostrar_productos_almacen_secundario():
    res = Response()
    content = request.json
    try:
        lista=[]

        query='SELECT ap.idalmacenproducto, prod.producto, ap.idalmacen, ap.minimo, ap.existente '\
        'FROM almacen_producto as ap, producto as prod '\
        "where prod.idproducto=ap.idproducto and ap.idalmacen='"+str(content['idalmacen'])+"' ORDER BY prod.idproducto ASC"

        for item in db.session.execute(query).all():
            lista.append({
                "idalmacenproducto":item['idalmacenproducto'],
                "producto":item['producto'],
                "idalmacen":item['idalmacen'],
                "minimo":float(item['minimo']),
                "existente":float(item['existente'])
            })
        
        res["ok"] = True
        res["data"] = lista

    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res    

@app.route('/public/almacen-principal/consultar-producto-almacen-principal', methods=['POST'])
def consultar_producto_almacen_principal():
    res = Response()
    try:

        content = request.json
        resultado={}

        producto=db.session.execute("select al.idalmacenprincipal,prod.codigo,prod.producto,al.minimo,al.existente "\
        "from producto as prod, almacen_principal as al "\
        "where al.idproducto=prod.idproducto and prod.codigo='"+content['codigo']+"'").first()

        if producto is not None:

            resultado={
                "idalmacenprincipal":producto.idalmacenprincipal,
                "codigo":producto.codigo,
                "producto":producto.producto,
                "minimo":str(producto.minimo),
                "existente":str(producto.existente)
            }

            res["ok"] = True
            res["data"] = resultado 

        else:

            res["ok"] = False
            res["msg"] = str("Producto No Existente.")
        
    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res

@app.route('/public/almacen-secundario/consultar-producto-almacen-secundario', methods=['POST'])
def consultar_producto_almacen_secundario():
    res = Response()
    try:

        content = request.json
        resultado={}

        producto=db.session.execute("select al.idalmacenproducto,prod.codigo,prod.producto,al.minimo,al.existente "\
        "from producto as prod, almacen_producto as al "\
        "where al.idproducto=prod.idproducto and prod.codigo='"+content['codigo']+"' and al.idalmacen="+str(content['idalmacen'])+" ").first()

        if producto is not None:

            resultado={
                "idalmacenproducto":producto.idalmacenproducto,
                "codigo":producto.codigo,
                "producto":producto.producto,
                "minimo":str(producto.minimo),
                "existente":str(producto.existente)
            }

            res["ok"] = True
            res["data"] = resultado 

        else:

            res["ok"] = False
            res["msg"] = str("Producto No Existente.")
        
    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res    

@app.route('/public/almacen/almacen-agregar', methods=['POST'])
def almacen_agregar():
    res = Response()
    try:

        content = request.json
        now = datetime.datetime.now(tz)

        addAlmacen=db.session.execute('INSERT INTO almacen(almacen, "fechaCreacion", "fechaActualizacion")'\
        "VALUES (UPPER('"+content['almacen']+"'), '"+str(now)+"', '"+str(now)+"')")
        db.session.commit()

        ultimoId=db.session.execute("SELECT idalmacen FROM almacen order by idalmacen desc limit 1").first()

        for item in db.session.execute("SELECT idproducto FROM producto").all():
            addAlmacenProducto=db.session.execute('INSERT INTO almacen_producto(idproducto, idalmacen, minimo, '\
            'existente, "fechaCreacion", "fechaActualizacion") '\
            "VALUES ("+str(item.idproducto)+", "+str(ultimoId.idalmacen)+", 0, 0, '"+str(now)+"', '"+str(now)+"')")
            db.session.commit()


    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res

@app.route('/public/almacen/almacen-sumar-existencia', methods=['POST'])
def almacen_sumar_existencia():
    res = Response()
    try:

        content = request.json
        now = datetime.datetime.now(tz)

        addAlmacen=db.session.execute('INSERT INTO almacen(almacen, "fechaCreacion", "fechaActualizacion")'\
        "VALUES (UPPER('"+content['almacen']+"'), '"+str(now)+"', '"+str(now)+"')")
        db.session.commit()

        ultimoId=db.session.execute("SELECT idalmacen FROM almacen order by idalmacen desc limit 1").first()

        for item in db.session.execute("SELECT idproducto FROM producto").all():
            addAlmacenProducto=db.session.execute('INSERT INTO almacen_producto(idproducto, idalmacen, minimo, '\
            'existente, "fechaCreacion", "fechaActualizacion") '\
            "VALUES ("+str(item.idproducto)+", "+str(ultimoId.idalmacen)+", 0, 0, '"+str(now)+"', '"+str(now)+"')")
            db.session.commit()


    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res

@app.route('/public/almacen/crear-lista', methods=['POST'])
def almacen_crear_lista():
    res = Response()
    try:

        content = request.json
        now = datetime.datetime.now(tz)

        addAlmacen=db.session.execute('INSERT INTO almacen_almacen(idalmacen, status, "fechaCreacion", "fechaActualizacion",alta)'\
        "VALUES ("+str(content['idalmacen'])+", null, '"+str(now)+"', '"+str(now)+"',null)")
        db.session.commit()
        
    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res

@app.route('/public/almacen/agregar-a-lista', methods=['POST'])
def agregar_producto_lista():
    res = Response()
    try:

        content = request.json
        now = datetime.datetime.now(tz)
        listaPaquete=[]
        infoProd=db.session.execute("SELECT pr.idproducto,ap.existente,pr.pza "\
        "FROM producto as pr, almacen_principal as ap where pr.idproducto=ap.idproducto and "\
        "pr.codigo='"+content['codigo']+"'").first()


        if infoProd is None:
            res["ok"] = False
            res['msg']=str("El codigo no existe.")
            return res
        
        if infoProd.existente < float(content['cantidad']) and infoProd.pza==1:
            res["ok"] = False
            res['msg']=str("Cantidad insuficiente. Existencia actual: "+str(infoProd.existente))
            return res

        if infoProd.pza==2:
            obtenerLista=db.session.execute("select pp.idpaqueteproducto,prod.idproducto,prod.producto, pp.cantidad,prod.precioproveedor,prod.preciocliente,prod.preciopublico "\
                    "from paquete_producto pp,producto prod where pp.idproducto="+str(infoProd.idproducto)+" and "\
                        "prod.idproducto=pp.idproductorelacion").all()

            for prod in obtenerLista:
                totalCantidad=float(prod.cantidad)*float(content['cantidad'])

                infoProdPaq=db.session.execute("SELECT pr.idproducto,ap.existente,pr.pza "\
                "FROM producto as pr, almacen_principal as ap where pr.idproducto=ap.idproducto and "\
                "pr.idproducto="+str(prod.idproducto)+" ").first()

                if infoProdPaq.existente < float(totalCantidad):
                    res["ok"] = False
                    res['msg']=str("Cantidad insuficiente. El paquete no está completo.")
                    return res

                listaPaquete.append({
                    "idProducto":prod.idproducto,
                    "cantidad":totalCantidad
                })

        if infoProd.pza==1:
            addListaAlmacen=db.session.execute('INSERT INTO lista_almacen( idalmacenalmacen, idproducto, cantidad)'\
            "VALUES ( "+str(content['idalmacen'])+", "+str(infoProd.idproducto)+", "+str(content['cantidad'])+")")
            db.session.commit()
            
            updateAlmacenPrincipal=db.session.execute("UPDATE almacen_principal SET "\
            'existente=existente-'+str(content['cantidad'])+ ' WHERE idproducto='+str(infoProd.idproducto)+'')
            db.session.commit()

        if infoProd.pza==2:
            for art in listaPaquete:
                addListaAlmacen=db.session.execute('INSERT INTO lista_almacen( idalmacenalmacen, idproducto, cantidad)'\
                "VALUES ( "+str(content['idalmacen'])+", "+str(art['idProducto'])+", "+str(art['cantidad'])+")")
                db.session.commit()
                
                updateAlmacenPrincipal=db.session.execute("UPDATE almacen_principal SET "\
                'existente=existente-'+str(art['cantidad'])+ ' WHERE idproducto='+str(art['idProducto'])+'')
                db.session.commit()            
        
    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res

@app.route('/public/almacen/quitar-producto-lista', methods=['POST'])
def quitar_producto_lista():
    res = Response()
    try:

        content = request.json
        now = datetime.datetime.now(tz)

        updateListaAlmacen=db.session.execute("UPDATE lista_almacen SET "\
        'cantidad=cantidad-'+str(content['cantidad'])+ ' WHERE idlistaalmacen='+str(content['idlista']))
        db.session.commit()

        updateAlmacenPrincipal=db.session.execute("UPDATE almacen_principal SET "\
        'existente=existente+'+str(content['cantidad'])+ ' WHERE idproducto='+str(content['idproducto']))
        db.session.commit()
        
    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res

@app.route('/public/almacen/obtener-ultima-lista')
def obtener_ultima_lista():
    res = Response()
    try:

        content = request.json
        now = datetime.datetime.now(tz)
        datosAlmacen={}
        ultimaLista=[]
        
        ultimaAltaAlmacen=db.session.execute('SELECT alal.idalmacenalmacen,al.almacen,al.idalmacen, alal."fechaCreacion" '\
        'FROM almacen_almacen as alal, almacen as al where alal.idalmacen=al.idalmacen and alta is null').first()
        
        if ultimaAltaAlmacen is None:
            res["ok"] = False
            res["msg"] = str("No Existe lista creada.")
            return res

        datosAlmacen=({
            "idAlmacen":ultimaAltaAlmacen.idalmacenalmacen,
            "idAlmacenSecundario":ultimaAltaAlmacen.idalmacen,
            "almacen":ultimaAltaAlmacen.almacen,
            "fecha":ultimaAltaAlmacen.fechaCreacion
        })
        for item in db.session.execute('SELECT la.idlistaalmacen,la.idproducto,prod.producto, la.cantidad'\
        ' FROM lista_almacen as la,almacen_almacen as alal,producto as prod where la.idproducto=prod.idproducto'\
        ' and la.idalmacenalmacen=alal.idalmacenalmacen and la.cantidad>0 and alal.alta is null ORDER BY la.idlistaalmacen DESC').all():
            ultimaLista.append({
                "idlista":item.idlistaalmacen,
                "idproducto":item.idproducto,
                "producto":item.producto,
                "cantidad":float(item.cantidad)
            })

        data=({
            "datosAlmacen":datosAlmacen,
            "lista":ultimaLista
        })
        res['data']=data
    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res

@app.route('/public/almacen/listas-creadas', methods=['POST'])
def obtener_listas_creadas():
    res = Response()
    try:

        content = request.json
        now = datetime.datetime.now(tz)
        listas=[]
        listasCreadasAlmacen=""

        if content['estatus']=="Sin Estatus":

            listasCreadasAlmacen='SELECT alal.idalmacenalmacen, al.idalmacen,al.almacen,alal.status, alal."fechaCreacion"'\
            "FROM almacen_almacen alal, almacen al where alal.idalmacen=al.idalmacen and status is null and alta=1 ORDER BY alal.idalmacenalmacen DESC"
               
        if content['estatus']=="Rechazado" or content['estatus']=="Aceptado":

            listasCreadasAlmacen='SELECT alal.idalmacenalmacen, al.idalmacen,al.almacen,alal.status, alal."fechaCreacion"'\
            "FROM almacen_almacen alal, almacen al where alal.idalmacen=al.idalmacen and status= '"+content['estatus']+"' and alta=1 ORDER BY alal.idalmacenalmacen DESC"
                                             
        for item in db.session.execute(listasCreadasAlmacen).all():

            listas.append({
                "idalmacenalmacen":item.idalmacenalmacen,
                "idalmacen":item.idalmacen,
                "estatus":item.status,
                "almacen":item.almacen,
                "fechaCreacion":str(item.fechaCreacion)
            })

        if listas is None:
            res["ok"] = False
            res["msg"] = str("No Existe listas creadas.")
            return res
       
        res['data']=listas
    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res

@app.route('/public/almacen/obtener-lista-producto', methods=['POST'])
def obtener_lista_producto():
    res = Response()
    try:

        content = request.json
        now = datetime.datetime.now(tz)
        listas=[]
        
        for item in db.session.execute('SELECT la.idlistaalmacen, prod.producto, la.cantidad '\
        'FROM lista_almacen la, producto prod where la.idalmacenalmacen='+str(content['idAlmacen'])+' and prod.idproducto=la.idproducto and la.cantidad>0 ORDER BY la.idlistaalmacen DESC').all():
            listas.append({
                "producto":item.producto,
                "cantidad":str(item.cantidad)
            })

        if listas is None:
            res["ok"] = False
            res["msg"] = str("No Contiene productos.")
            return res
        
        res["ok"] = True
        res['data']=listas

    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res    

@app.route('/public/almacen/finalizar-lista', methods=['POST'])
def almacen_finalizar_lista():
    res = Response()
    try:

        content = request.json
        now = datetime.datetime.now(tz)

        addAlmacen=db.session.execute("UPDATE almacen_almacen SET alta=1,"+'"fechaActualizacion"'+"='"+str(now)+"' WHERE idalmacenalmacen='"+str(content['idalmacen'])+"'")
        db.session.commit()

        res["ok"] = True
        res["msg"] = str("La lista a finalizado exitosamente.")

    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res
    
@app.route('/public/almacen/cambiar-estatus', methods=['POST'])
def cambiar_estatus():
    res = Response()
    try:

        content = request.json
        now = datetime.datetime.now(tz)
        listas=[]

        addAlmacen=db.session.execute("UPDATE almacen_almacen SET status='"+content['estatus']+"',"+'"fechaActualizacion"'+"='"+str(now)+"' WHERE idalmacenalmacen='"+str(content['idAlmacenalmacen'])+"'")
        db.session.commit()

        if content['estatus']=="Aceptado":
            for item in db.session.execute('SELECT la.idproducto,la.cantidad '\
            'FROM lista_almacen la, producto prod where la.idalmacenalmacen='+str(content['idAlmacenalmacen'])+' and prod.idproducto=la.idproducto and la.cantidad>0').all():
                db.session.execute("UPDATE public.almacen_producto SET "\
                "existente=existente+"+str(item.cantidad)+" "\
                "WHERE idalmacen="+str(content['idAlmacen'])+" and idproducto="+str(item.idproducto)+" ")
                db.session.commit()                

        res["ok"] = True
        res["msg"] = str("La lista cambio su estatus.")

    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res    

@app.route('/public/almacen/cambiar-propietario', methods=['POST'])
def cambiar_propietario_almacen():
    res = Response()
    try:

        content = request.json
        now = datetime.datetime.now(tz)

        updateAlmacen=db.session.execute("UPDATE almacen_almacen SET idalmacen="+str(content['idalmacen'])+" WHERE idalmacenalmacen="+str(content['idalmacenalmacen'])+" ")
        db.session.commit()

        res["ok"] = True
        res["msg"] = str("La lista se actualizo exitosamente.")

    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res

@app.route('/public/almacen/mostrar-almacen')
def mostrar_almacen():
    res = Response()
    try:

        content = request.json
        now = datetime.datetime.now(tz)

        listaAlmacen=[]

        for item in db.session.execute('SELECT idalmacen, almacen,"fechaCreacion" FROM almacen ORDER BY idalmacen DESC').all():
            listaAlmacen.append({
                "idalmacen":item.idalmacen,
                "almacen":item.almacen,
                "fecha":str(item.fechaCreacion)
            })

        res["data"] = listaAlmacen

    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res  

# # # # # # categoria # # # # #   
@app.route('/public/categoria/obtener-categorias-activas')
def obtener_categoriasActivas():
    res = Response()
    try:

        now = datetime.datetime.now(tz)
        listas=[]
        
        for item in db.session.execute('SELECT idcategoria, categoria, alta, "fechaCreacion", "fechaActualizacion" FROM categoria where alta=1 order by idcategoria DESC').all():
            listas.append({
                "idcategoria":item.idcategoria,
                "categoria":item.categoria,
                "alta":item.alta,
                "fechaCreacion":str(item.fechaCreacion),
                "fechaActualizacion":str(item.fechaActualizacion)
            })

        if listas is None:
            res["ok"] = False
            res["msg"] = str("No Contiene categorias.")
            return res
        
        res["ok"] = True
        res['data']=listas

    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res

@app.route('/public/categoria/obtener-categorias')
def obtener_categorias():
    res = Response()
    try:

        now = datetime.datetime.now(tz)
        listas=[]
        
        for item in db.session.execute('SELECT idcategoria, categoria, alta, "fechaCreacion", "fechaActualizacion" FROM categoria order by idcategoria DESC').all():
            listas.append({
                "idcategoria":item.idcategoria,
                "categoria":item.categoria,
                "alta":item.alta,
                "fechaCreacion":str(item.fechaCreacion),
                "fechaActualizacion":str(item.fechaActualizacion)
            })

        if listas is None:
            res["ok"] = False
            res["msg"] = str("No Contiene categorias.")
            return res
        
        res["ok"] = True
        res['data']=listas

    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res    

@app.route('/public/categoria/agregar-categoria', methods=['POST'])
def agregar_categoria():
    res = Response()
    try:

        content = request.json
        now = datetime.datetime.now(tz)

        updateAlmacen=db.session.execute('INSERT INTO categoria(categoria, alta,'\
        ' "fechaCreacion", "fechaActualizacion") '\
        "VALUES ( UPPER('"+content['categoria']+"'), 1, '"+str(now)+"', '"+str(now)+"')")
        db.session.commit()

        res["ok"] = True
        res["msg"] = str("La categoria se agrego exitosamente.")

    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res

@app.route('/public/categoria/editar-categoria', methods=['POST'])
def editar_categoria():
    res = Response()
    try:

        content = request.json
        now = datetime.datetime.now(tz)

        updateAlmacen=db.session.execute("UPDATE categoria SET categoria=UPPER('"+content['categoria']+"'),"\
        '"fechaActualizacion"'+"='"+str(now)+"' WHERE idcategoria="+str(content['idcategoria'])+" ")
        db.session.commit()

        res["ok"] = True
        res["msg"] = str("La categoria se actualizo exitosamente.")

    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res

@app.route('/public/categoria/estatus-categoria', methods=['POST'])
def eliminar_categoria():
    res = Response()
    try:

        content = request.json
        now = datetime.datetime.now(tz)

        updateAlmacen=db.session.execute("UPDATE categoria SET alta="+str(content['alta'])+" "\
        "WHERE idcategoria="+str(content['idcategoria'])+" ")
        db.session.commit()

        res["ok"] = True
        res["msg"] = str("La categoria se actualizo exitosamente.")

    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res

# # # # # empleado # # # # #
@app.route('/public/empleado/editar-empleado', methods=['POST'])
def editar_empleado():
    res = Response()
    try:

        content = request.json
        now = datetime.datetime.now(tz)

        existente=db.session.execute("SELECT id FROM usuario where username='"+content['usuario']+"'").first()
        db.session.commit()

        if existente is None or (existente.id==content['idusuario']):
            updateEmpleado=db.session.execute("UPDATE usuario SET nombre=UPPER('"+content['nombre']+"'), domicilio=UPPER('"+content['domicilio']+"'),"
            " username='"+content['usuario']+"', password='"+content['password']+"', idrol="+str(content['idrole'])+","\
            ' "fechaActualizacion"= '+" '"+str(now)+"' WHERE id="+str(content['idusuario'])+" ")
            db.session.commit()

            res["ok"] = True
            res["msg"] = str("El empleado se Actualizo exitosamente.")
        else:
            res["ok"] = False
            res["msg"] = str("Usuario no disponible.")
    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res

@app.route('/public/empleado/agregar-empleado', methods=['POST'])
def agregar_empleado():
    res = Response()
    try:

        content = request.json
        now = datetime.datetime.now(tz)

        existente=db.session.execute("SELECT id FROM usuario where username='"+content['usuario']+"'").first()
        db.session.commit()

        if existente is None:
            updateAlmacen=db.session.execute('INSERT INTO usuario(nombre, domicilio, username, password, idrol,alta,'\
            ' "fechaCreacion", "fechaActualizacion") '\
            "VALUES (UPPER('"+content['nombre']+"'), UPPER('"+content['domicilio']+"'),'"+content['usuario']+"', '"+content['password']+"', "+str(content['idrole'])+", 1,"\
            " '"+str(now)+"', '"+str(now)+"')")
            db.session.commit()

            res["ok"] = True
            res["msg"] = str("El empleado se agrego exitosamente.")
        else:
            res["ok"] = False
            res["msg"] = str("Usuario no disponible.")

    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res

@app.route('/public/empleado/obtener-empleados')
def obtener_empleados():
    res = Response()
    try:

        now = datetime.datetime.now(tz)
        listas=[]
        
        for item in db.session.execute('select us.id,us.alta,us.nombre,us.domicilio,us.username,us.password,'\
        'rol.role,us.idrol,"fechaCreacion","fechaActualizacion" from usuario as us,role as rol '\
        'where rol.idrole=us.idrol order by id DESC').all():
            listas.append({
                "id":item.id,
                "nombre":item.nombre,
                "domicilio":item.domicilio,
                "usuario":item.username,
                "password":item.password,
                "alta":item.alta,
                "role":item.role,
                "idrole":item.idrol,
                "fechaCreacion":str(item.fechaCreacion),
                "fechaActualizacion":str(item.fechaActualizacion)
            })

        if listas is None:
            res["ok"] = False
            res["msg"] = str("No Contiene Empleados.")
            return res
        
        res["ok"] = True
        res['data']=listas

    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res

# # # # # role # # # # #
@app.route('/public/role/obtener-roles')
def obtener_roles():
    res = Response()
    try:

        now = datetime.datetime.now(tz)
        listas=[]
        
        for item in db.session.execute('SELECT idrole, role, agregarproducto, editarproducto,'\
        ' eliminarproducto, agregaralmacen, editaralmacen, eliminaralmacen, agregarcategoria, '\
        'editarcategoria, eliminarcategoria, agregarproductoap, ajustarproductoap, generarlista '\
        'FROM public.role order by idrole DESC').all():
            listas.append({
                "idrole":item.idrole,
                "role":item.role,
                "agregarproducto":item.agregarproducto,
                "editarproducto":item.editarproducto,
                "eliminarproducto":item.eliminarproducto,
                "agregaralmacen":item.agregaralmacen,
                "editaralmacen":item.editaralmacen,
                "eliminaralmacen":item.eliminaralmacen,
                "agregarcategoria":item.agregarcategoria,
                "editarcategoria":item.editarcategoria,
                "eliminarcategoria":item.eliminarcategoria,
                "agregarproductoap":item.agregarproductoap,
                "ajustarproductoap":item.ajustarproductoap,
                "generarlista":item.generarlista
            })

        if listas is None:
            res["ok"] = False
            res["msg"] = str("No Contiene Roles.")
            return res
        
        res["ok"] = True
        res['data']=listas

    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res

# # # # # tipo-cirugia # # # # #
@app.route('/public/tipo-cirugia/crear-tipo-cirugia', methods=['POST'])
def guardar_tipo_cirugia():
    res = Response()
    try:

        content = request.json
        now = datetime.datetime.now(tz)

        existente=db.session.execute("INSERT INTO tipo_cirugia(descripcion) "\
            "VALUES (UPPER('"+content['tipoCirugia']+"'));")
        db.session.commit()

        res["ok"] = True
        res["msg"] = str("Tipo Cirugia Guardada exitosamente.")
    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res

@app.route('/public/tipo-cirugia/editar-tipo-cirugia', methods=['POST'])
def editar_tipo_cirugia():
    res = Response()
    try:

        content = request.json
        now = datetime.datetime.now(tz)

        existente=db.session.execute("UPDATE tipo_cirugia SET descripcion=UPPER('"+content['tipoCirugia']+"') "\
            "WHERE idtipocirugia="+str(content['idTipoCirugia'])+" ;")
        db.session.commit()

        res["ok"] = True
        res["msg"] = str("Tipo de Cirgía actualizada correctamente.")
    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res

@app.route('/public/tipo-cirugia/obtener-tipo-cirugias')
def obtener_tipo_cirugias():
    res = Response()
    try:

        now = datetime.datetime.now(tz)
        listas=[]
        
        for item in db.session.execute('SELECT idtipocirugia, descripcion FROM tipo_cirugia order by idtipocirugia DESC').all():
            listas.append({
                "idTipoCirugia":item.idtipocirugia,
                "tipoCirugia":item.descripcion
            })

        if listas is None:
            res["ok"] = False
            res["msg"] = str("No Contiene Tipo de Cirugias.")
            return res
        
        res["ok"] = True
        res['data']=listas

    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res

# # # # # paciente # # # # #
@app.route('/public/paciente/crear-paciente', methods=['POST'])
def guardar_paciente():
    res = Response()
    try:

        content = request.json
        now = datetime.datetime.now(tz)

        existente=db.session.execute("INSERT INTO paciente(paciente, telefono, edad)"\
            " VALUES ( UPPER('"+content['paciente']+"'), '"+content['telefono']+"', '"+content['edad']+"');")
        db.session.commit()

        res["ok"] = True
        res["msg"] = str("Paciente Guardado exitosamente.")
    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res

@app.route('/public/paciente/editar-paciente', methods=['POST'])
def editar_paciente():
    res = Response()
    try:

        content = request.json
        now = datetime.datetime.now(tz)

        existente=db.session.execute("UPDATE paciente SET paciente=UPPER('"+content['paciente']+"'), "\
            "telefono='"+content['telefono']+"', edad='"+content['edad']+"' WHERE idpaciente="+str(content['idPaciente'])+";")
        db.session.commit()

        res["ok"] = True
        res["msg"] = str("Paciente actualizado correctamente.")
    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res

@app.route('/public/paciente/obtener-pacientes')
def obtener_pacientes():
    res = Response()
    try:

        now = datetime.datetime.now(tz)
        listas=[]
        
        for item in db.session.execute('SELECT idpaciente, paciente, telefono, edad FROM paciente order by idpaciente DESC').all():
            listas.append({
                "idPaciente":item.idpaciente,
                "paciente":item.paciente,
                "telefono":item.telefono,
                "edad":item.edad
            })

        if listas is None:
            res["ok"] = False
            res["msg"] = str("No Contiene Pacientes.")
            return res
        
        res["ok"] = True
        res['data']=listas

    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res

# # # # # doctor # # # # #
@app.route('/public/doctor/crear-doctor', methods=['POST'])
def guardar_doctor():
    res = Response()
    try:

        content = request.json
        now = datetime.datetime.now(tz)

        existente=db.session.execute("INSERT INTO doctor(doctor, telefono, interno, alta, fechacreacion,"\
            " fechaactualizacion) VALUES ( UPPER('"+content['doctor']+"'), '"+content['telefono']+"',"\
                " "+str(content['interno'])+", 1, '"+str(now)+"', '"+str(now)+"');")
        db.session.commit()

        res["ok"] = True
        res["msg"] = str("Doctor Guardado exitosamente.")
    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res

@app.route('/public/doctor/editar-doctor', methods=['POST'])
def editar_doctor():
    res = Response()
    try:

        content = request.json
        now = datetime.datetime.now(tz)

        existente=db.session.execute("UPDATE doctor SET doctor=UPPER('"+content['doctor']+"'), "\
            "telefono='"+content['telefono']+"', interno="+str(content['interno'])+", "\
                "fechaactualizacion='"+str(now)+"' WHERE iddoctor="+str(content['idDoctor'])+";")
        db.session.commit()

        res["ok"] = True
        res["msg"] = str("Doctor actualizado correctamente.")
    except Exception as e:
        print(e)
        res["ok"] = False
        res["msg"] = str(e)

    return res

@app.route('/public/doctor/alta-baja-doctor', methods=['POST'])
def alta_baja_doctor():
    res = Response()
    try:

        content = request.json
        now = datetime.datetime.now(tz)

        existente=db.session.execute("UPDATE doctor SET alta="+str(content['alta'])+", "\
            "fechaactualizacion='"+str(now)+"' WHERE iddoctor="+str(content['idDoctor'])+";")
        db.session.commit()

        res["ok"] = True
        res["msg"] = str("Doctor actualizado correctamente.")
    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res

@app.route('/public/doctor/obtener-doctores')
def obtener_doctor():
    res = Response()
    try:

        now = datetime.datetime.now(tz)
        listas=[]
        
        for item in db.session.execute('SELECT iddoctor, doctor, telefono, interno, alta,fechacreacion,fechaactualizacion FROM doctor order by iddoctor DESC').all():
            listas.append({
                "idDoctor":item.iddoctor,
                "doctor":item.doctor,
                "telefono":item.telefono,
                "interno":item.interno,
                "alta":item.alta,
                "fechaCreacion":str(item.fechacreacion),
                "fechaActualizacion":str(item.fechaactualizacion)
            })

        if listas is None:
            res["ok"] = False
            res["msg"] = str("No Contiene Doctores.")
            return res
        
        res["ok"] = True
        res['data']=listas

    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res

# # # # # almacen_paciente # # # # #
@app.route('/public/almacen-paciente/crear-almacen-paciente', methods=['POST'])
def guardar_almacen_paciente():
    res = Response()
    try:

        content = request.json
        now = datetime.datetime.now(tz)
        informacion={}

        insertar=db.session.execute("INSERT INTO almacen_paciente(enfermera, idalmacen, "\
            "idtipocirugia, idpaciente, iddoctor, fechacreacion, status) "\
                "VALUES (UPPER('"+content['enfermera']+"'),"+str(content['idAlmacen'])+" ,"+str(content['idTipoCirugia'])+" "\
                    ","+str(content['idPaciente'])+" , "+str(content['idDoctor'])+", '"+str(now)+"', 'Proceso');")
        db.session.commit()

        existente=db.session.execute("SELECT idalmacenpaciente, enfermera, idalmacen,"\
            " idtipocirugia, idpaciente, iddoctor, status FROM almacen_paciente "\
                "where idalmacen="+str(content['idAlmacen'])+" and status='Proceso';").first()
        db.session.commit()

        if existente is not None:
            informacion.update({
                "idAlmacenPaciente":existente.idalmacenpaciente,
                "enfermera":existente.enfermera,
                "idAlmacen":existente.idalmacen,
                "idTipoCirugia":existente.idtipocirugia,
                "idPaciente":existente.idpaciente,
                "idDoctor":existente.iddoctor,
                "status":existente.status
            })

        res["ok"] = True
        res["data"]=informacion
        res["msg"] = str("Relacion de reporte Generado.")
    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res

@app.route('/public/almacen-paciente/editar-almacen-paciente', methods=['POST'])
def editar_almacen_paciente():
    res = Response()
    try:

        content = request.json
        now = datetime.datetime.now(tz)

        actualizar=db.session.execute("UPDATE almacen_paciente SET enfermera=UPPER('"+content['enfermera']+"'), "\
            "idtipocirugia="+str(content['idTipoCirugia'])+" ,"\
                " idpaciente="+str(content['idPaciente'])+" , "\
                    "iddoctor="+str(content['idDoctor'])+" "\
                        "WHERE idalmacenpaciente="+str(content['idAlmacenPaciente'])+" ;")
        db.session.commit()

        res["ok"] = True
        res["msg"] = str("Relacion de reporte Actualizado.")
    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res

@app.route('/public/almacen-paciente/seleccionar-almacen-paciente', methods=['POST'])
def seleccionar_almacen_paciente():
    res = Response()
    try:

        content = request.json
        now = datetime.datetime.now(tz)
        informacion={}
        existente=db.session.execute("SELECT idalmacenpaciente, enfermera, idalmacen,"\
            " idtipocirugia, idpaciente, iddoctor, status FROM almacen_paciente "\
                "where idalmacen="+str(content['idAlmacen'])+" and status='Proceso';").first()
        db.session.commit()

        if existente is not None:
            informacion.update({
                "idAlmacenPaciente":existente.idalmacenpaciente,
                "enfermera":existente.enfermera,
                "idAlmacen":existente.idalmacen,
                "idTipoCirugia":existente.idtipocirugia,
                "idPaciente":existente.idpaciente,
                "idDoctor":existente.iddoctor,
                "status":existente.status
            })

            res["ok"] = True
            res["data"]=informacion
            res["msg"] = str("Informacion de almacen obtenida.")
        else:
            res["ok"] = False
            res["msg"] = str("No existe nada en proceso en este almacen.")

    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res

# # # # # almacen_lista_paciente # # # # #

@app.route('/public/almacen-lista-paciente/agregar-a-lista-paciente', methods=['POST'])
def agregar_producto_lista_paciente():
    res = Response()
    try:

        content = request.json
        now = datetime.datetime.now(tz)
        listaPaquete=[]
        
        infoProd=db.session.execute("SELECT pr.idproducto,ap.existente,pr.pza "\
        "FROM producto as pr, almacen_producto as ap where pr.idproducto=ap.idproducto and "\
        "pr.codigo='"+content['codigo']+"' and ap.idalmacen="+str(content['idAlmacen'])+" ").first()


        if infoProd is None:
            res["ok"] = False
            res['msg']=str("El codigo no existe.")
            return res
        
        if infoProd.existente < float(content['cantidad']) and infoProd.pza==1:
            res["ok"] = False
            res['msg']=str("Cantidad insuficiente. Existencia actual: "+str(infoProd.existente))
            return res

        if infoProd.pza==2:
            obtenerLista=db.session.execute("select pp.idpaqueteproducto,prod.idproducto,prod.producto, pp.cantidad,prod.precioproveedor,prod.preciocliente,prod.preciopublico "\
                    "from paquete_producto pp,producto prod where pp.idproducto="+str(infoProd.idproducto)+" and "\
                        "prod.idproducto=pp.idproductorelacion").all()

            for prod in obtenerLista:
                totalCantidad=float(prod.cantidad)*float(content['cantidad'])

                infoProdPaq=db.session.execute("SELECT pr.idproducto,ap.existente,pr.pza "\
                "FROM producto as pr, almacen_producto as ap where pr.idproducto=ap.idproducto and "\
                "pr.idproducto="+str(prod.idproducto)+" and ap.idalmacen="+str(content['idAlmacen'])+" ").first()

                if infoProdPaq.existente < float(totalCantidad):
                    res["ok"] = False
                    res['msg']=str("Cantidad insuficiente. El paquete no está completo.")
                    return res

                listaPaquete.append({
                    "idProducto":prod.idproducto,
                    "cantidad":totalCantidad
                }) 

        if infoProd.pza==1:                       
            addListaAlmacen=db.session.execute("INSERT INTO lista_almacen_salida(idalmacenpaciente, idproducto, cantidad)VALUES ( '"+str(content['idAlmacenPaciente'])+"',"+str(infoProd.idproducto)+" , "+str(content['cantidad'])+");")
            db.session.commit()

            updateAlmacenPrincipal=db.session.execute("UPDATE almacen_producto SET "\
            'existente=existente-'+str(content['cantidad'])+ ' WHERE idproducto='+str(infoProd.idproducto)+' and idalmacen='+str(content['idAlmacen'])+' ')
            db.session.commit()

        if infoProd.pza==2:
            for art in listaPaquete:
                addListaAlmacen=db.session.execute("INSERT INTO lista_almacen_salida(idalmacenpaciente, idproducto, cantidad)VALUES ( '"+str(content['idAlmacenPaciente'])+"',"+str(art['idProducto'])+" , "+str(art['cantidad'])+");")
                db.session.commit()

                updateAlmacenPrincipal=db.session.execute("UPDATE almacen_producto SET "\
                'existente=existente-'+str(art['cantidad'])+ ' WHERE idproducto='+str(art['idProducto'])+' and idalmacen='+str(content['idAlmacen'])+' ')
                db.session.commit()

    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res

@app.route('/public/almacen-lista-paciente/obtener-lista-almacen-paciente', methods=['POST'])
def obtener_lista_almacen_paciente():
    res = Response()
    try:

        content = request.json
        now = datetime.datetime.now(tz)
        listas=[]
               
        for item in db.session.execute("SELECT las.idlistaalmacensalida, las.idproducto,"\
            " las.cantidad,prod.producto FROM lista_almacen_salida las,producto prod "\
                "WHERE las.cantidad!=0 and las.idalmacenpaciente="+str(content['idAlmacenPaciente'])+" and prod.idproducto=las.idproducto "\
                    "order by las.idlistaalmacensalida desc;").all():
            listas.append({
                "idlista":item.idlistaalmacensalida,
                "idproducto":item.idproducto,
                "producto":item.producto,
                "cantidad":float(item.cantidad)
            })


        if listas is None:
            res["ok"] = False
            res["msg"] = str("Lista vacia.")
            return res
        
        res["ok"] = True
        res['data']=listas

    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res  

@app.route('/public/almacen-lista-paciente/quitar-producto-lista-paciente', methods=['POST'])
def quitar_producto_lista_almacen_paciente():
    res = Response()
    try:

        content = request.json
        now = datetime.datetime.now(tz)

        updateListaAlmacen=db.session.execute("UPDATE lista_almacen_salida SET "\
        'cantidad=cantidad-'+str(content['cantidad'])+ ' WHERE idlistaalmacensalida='+str(content['idlista']))
        db.session.commit()

        updateAlmacenPrincipal=db.session.execute("UPDATE almacen_producto SET "\
        'existente=existente+'+str(content['cantidad'])+ ' WHERE idalmacen='+str(content['idalmacen'])+' and idproducto='+str(content['idproducto']))
        db.session.commit()
        
    except Exception as e:
        res["ok"] = False
        res["msg"] = str(e)

    return res

@app.route('/public/almacen-lista-paciente/finalizar-lista-paciente', methods=['POST'])
def finalizar_lista_almacen_paciente():
    res = Response()
    try:

        content = request.json
        now = datetime.datetime.now(tz)
        
        listas=[]
        resultado={}

        updateListaAlmacen=db.session.execute("UPDATE almacen_paciente SET status='Finalizado' WHERE idalmacenpaciente="+str(content['idAlmacenPaciente'])+";")
        db.session.commit()
              
        listaAlmacenSalida=db.session.execute("select tc.descripcion,pac.paciente,pac.telefono,pac.edad,doc.doctor,ap.enfermera,al.almacen,TO_CHAR(ap.fechacreacion,'YYYY-MM-DD HH:mm:ss') as fechacreacion "\
            "from tipo_cirugia tc,paciente pac,doctor doc,almacen_paciente ap, almacen al "\
                "WHERE tc.idtipocirugia=ap.idtipocirugia and pac.idpaciente=ap.idpaciente and doc.iddoctor=ap.iddoctor "\
                    "and al.idalmacen=ap.idalmacen and idalmacenpaciente="+str(content['idAlmacenPaciente'])+" ").first()

        if listaAlmacenSalida is not None:

            resultado={
                "tipoCirugia":listaAlmacenSalida.descripcion,
                "paciente":listaAlmacenSalida.paciente,
                "doctor":listaAlmacenSalida.doctor,
                "enfermera":listaAlmacenSalida.enfermera,
                "almacen":listaAlmacenSalida.almacen,
                "fechaCreacion":str(listaAlmacenSalida.fechacreacion),
                "telefono":listaAlmacenSalida.telefono,
                "edad":listaAlmacenSalida.edad
            }   

            for item in db.session.execute("SELECT las.idlistaalmacensalida, las.idproducto,"\
                " las.cantidad,prod.producto FROM lista_almacen_salida las,producto prod "\
                    "WHERE las.cantidad!=0 and las.idalmacenpaciente="+str(content['idAlmacenPaciente'])+" and prod.idproducto=las.idproducto "\
                        "order by las.idlistaalmacensalida desc;").all():
                listas.append({
                    "idlista":item.idlistaalmacensalida,
                    "idproducto":item.idproducto,
                    "producto":item.producto,
                    "cantidad":float(item.cantidad)
                })

            resultado.update({"listaAlmacen":listas})

            res['ok']=True
            res['data']=resultado
            res['msg']="Lista finalizada."
        else:
            res['ok']=False
            res['msg']="Lista no encontrada."


    except Exception as e:
        print(e)
        res["ok"] = False
        res["msg"] = str(e)

    return res    

@app.route('/public/almacen-lista-paciente/lista-paciente-finalizadas', methods=['POST'])
def obtener_lista_almacen_paciente_finalizadas():
    res = Response()
    try:

        content = request.json
        now = datetime.datetime.now(tz)
        
        resultado=[]
        listas=[]
              
        listaAlmacen=db.session.execute("select ap.idalmacenpaciente,tc.descripcion,pac.paciente,pac.telefono,pac.edad,doc.doctor,ap.enfermera,al.almacen,TO_CHAR(ap.fechacreacion,'YYYY-MM-DD HH:mm:ss') as fechacreacion,ap.status "\
            "from tipo_cirugia tc,paciente pac,doctor doc,almacen_paciente ap, almacen al "\
                "WHERE tc.idtipocirugia=ap.idtipocirugia and pac.idpaciente=ap.idpaciente and doc.iddoctor=ap.iddoctor "\
                    "and al.idalmacen=ap.idalmacen and ap.idalmacen="+str(content['idAlmacen'])+" ORDER BY ap.fechacreacion DESC").all()

        if listaAlmacen is not None:

            for listaAlmacenSalida in listaAlmacen:
                listas=[]
                for item in db.session.execute("SELECT las.idlistaalmacensalida, las.idproducto,"\
                    " las.cantidad,prod.producto FROM lista_almacen_salida las,producto prod "\
                        "WHERE las.cantidad!=0 and las.idalmacenpaciente="+str(listaAlmacenSalida.idalmacenpaciente)+" and prod.idproducto=las.idproducto "\
                            "order by las.idlistaalmacensalida desc;").all():
                    listas.append({
                        "idlista":item.idlistaalmacensalida,
                        "idproducto":item.idproducto,
                        "producto":item.producto,
                        "cantidad":float(item.cantidad)
                    })

                resultado.append({
                    "idAlmacenPaciente":listaAlmacenSalida.idalmacenpaciente,
                    "tipoCirugia":listaAlmacenSalida.descripcion,
                    "paciente":listaAlmacenSalida.paciente,
                    "doctor":listaAlmacenSalida.doctor,
                    "enfermera":listaAlmacenSalida.enfermera,
                    "almacen":listaAlmacenSalida.almacen,
                    "fechaCreacion":str(listaAlmacenSalida.fechacreacion),
                    "telefono":listaAlmacenSalida.telefono,
                    "edad":listaAlmacenSalida.edad,
                    "status":listaAlmacenSalida.status,
                    "listaAlmacen":listas
                })

            res['ok']=True
            res['data']=resultado
            res['msg']="Lista finalizada."
        else:
            res['ok']=False
            res['msg']="Lista no encontrada."


    except Exception as e:
        print(e)
        res["ok"] = False
        res["msg"] = str(e)

    return res  

# # # # # private methods # # # # #

dictConfig({
    'version': 1,
    'formatters': {'default': {
        'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
    }},
    'handlers': {'wsgi': {
        'class': 'logging.StreamHandler',
        'stream': 'ext://flask.logging.wsgi_errors_stream',
        'formatter': 'default'
    }},
    'root': {
        'level': 'INFO',
        'handlers': ['wsgi']
    }
})

def Response():
    return {"ok": True, "msg": None, "data": None}

if __name__ == '__main__':
    app.run(host="0.0.0.0", port="5000")
